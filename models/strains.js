var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var strainSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    category: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    fiveStarRating: {
        type: Number,
        required: true,
    },
    flavors: {
        type: [String],
        required: true,
    },
    lineages: {
        type: [String],
        required: true
    },
    
    medicalAttributes: {
        type: [String],
        required: true,
    },
    negativeAttributes: {
        type: [String],
        required: true,
    },
    photos: {
        type: [String],
        required: false,
    },
    positiveAttributes: {
        type: [String],
        required: true,
    },
    videos: {
        type: [String],
        required: false,
    }

}, {
    timestamps: true
});

// the schema is useless so far
// we need to create a model using it
var Strains = mongoose.model('Strain', strainSchema);

// make this available to our Node applications
module.exports = Strains;