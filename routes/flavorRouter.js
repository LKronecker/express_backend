var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var router = express.Router();
router.use(bodyParser.json());

// create a schema
var flavorSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
}, {
    timestamps: true
});

// create model
var Flavors = mongoose.model('Flavor', flavorSchema);

router.route('/')
.get(function (req, res, next) {
    Flavors.find({}, function (err, flavor) {
        if (err){
            return next(err);
        }
        res.json(flavor);
    });
})

module.exports = router;