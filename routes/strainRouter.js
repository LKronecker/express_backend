var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Verify = require('./verify');
var Strains = require('../models/strains');

var router = express.Router();
router.use(bodyParser.json());

router.route('/')
.get(function (req, res, next) {
    Strains.find(req.query)
        .exec(function (err, strain) {
        if (err) next(err);
        res.json(strain);
    });
    // Strains.find({}, function (err, strain) {
    //     if (err){
    //         return next(err);
    //     }
    //     res.json(strain);
    // });

})

.post(Verify.verifyOrdinaryUser, function (req, res, next) {
    Strains.create(req.body, function (err, strain) {
        if (err){
            return next(err);
        }
        console.log('Strain created!');
        var id = strain._id;

        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.end('Added the strain with id: ' + id);
    });
})

.delete(Verify.verifyOrdinaryUser, Verify.verifyAdmin, function (req, res, next) {
    res.end('Data deleting not supported through this API. Delete from the db directly');
//    Uncomment the code below to delete the ENTIRE collection of strains
//    Strains.remove({}, function (err, resp) {
//        if (err){
//            return next(err);
//        }
//        res.json(resp);
//    });
});

router.route('/:strainId')
.get(function (req, res, next) {
    Strains.findById(req.params.strainId, function (err, strain) {
        if (err){
            return next(err);
        }
        res.json(strain);
    });
})

.put(function (req, res, next) {
    Strains.findByIdAndUpdate(req.params.strainId, {
        $set: req.body
    }, {
        new: true
    }, function (err, strain) {
        if (err){
            return next(err);
        }
        res.json(strain);
    });
})

.delete(function (req, res, next) {
    res.end('Data deleting not supported through this API. Delete from the db directly');
//    Uncomment the code below to delete the ONE strain given it's ID.
//    Strains.findByIdAndRemove(req.params.strainId, function (err, resp) {        
//        if (err){
//            return next(err);
//        }
//        res.json(resp);
//    });
});

module.exports = router;