var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var router = express.Router();
router.use(bodyParser.json());

// create a schema
var negativeSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
}, {
    timestamps: true
});

var NegativeEff = mongoose.model('Negative', negativeSchema);

router.route('/')
.get(function (req, res, next) {
    NegativeEff.find({}, function (err, negative) {
        if (err){
            return next(err);
        }
        res.json(negative);
    });
})

module.exports = router;