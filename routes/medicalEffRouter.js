var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var router = express.Router();
router.use(bodyParser.json());

// create a schema
var medicalSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
}, {
    timestamps: true
});

var MedicalEff = mongoose.model('Medical', medicalSchema);

router.route('/')
.get(function (req, res, next) {
    MedicalEff.find({}, function (err, medical) {
        if (err){
            return next(err);
        }
        res.json(medical);
    });
})

module.exports = router;