var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var router = express.Router();
router.use(bodyParser.json());

// create a schema
var positiveSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
}, {
    timestamps: true
});

var PositiveEff = mongoose.model('Positive', positiveSchema);

router.route('/')
.get(function (req, res, next) {
    PositiveEff.find({}, function (err, positive) {
        if (err){
            return next(err);
        }
        res.json(positive);
    });
})

module.exports = router;