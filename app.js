///////////////////////// Database ///////////////////////

var util = require('util');
var async = require('async');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var config = require('./config');

//Connection
var loginCredentials = config.dbUser + ":" + config.dbPassword;
var db = mongoose.connection;

mongoose.connect("mongodb://" + loginCredentials + "@" + config.dbUrl + "/" + config.dbName);

db.on("open", function() {
    console.log("connection to database succesfull!!!");
});

db.on("error", function() {
    console.log("error");
});

/////////////////////// Express server /////////////////

var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var bootstrap = require("express-bootstrap-service");

// Authentication
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// routes
var routes = require('./routes/index');
var users = require('./routes/users');
var strains = require('./routes/strainRouter');
var flavors = require('./routes/flavorRouter');
var medical = require('./routes/medicalEffRouter');
var negative = require('./routes/negativeEffRouter');
var positive = require('./routes/positiveEffRouter');
//For html views
var ejs = require('ejs');

// Init express
var app = express();

// Secure traffic only
app.all('*', function(req, res, next){
    console.log('req start: ',req.secure, req.hostname, req.url, app.get('port'));
  if (req.secure) {
    return next();
  };

 res.redirect('https://'+req.hostname+':'+app.get('secPort')+req.url);
});

// view engine setup to render bootstrap
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'html');
app.engine('html', ejs.renderFile);


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bootstrap.serve);

// passport config
var User = require('./models/user');
app.use(passport.initialize());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// Make our db accessible to our router
app.use(function(req,res,next){
    req.db = db;
    next();
})

app.use('/', routes);
app.use('/users', users);
app.use('/strains', strains);
app.use('/flavors', flavors);
app.use('/medical', medical);
app.use('/negative', negative);
app.use('/positive', positive);

//use static files from the public folder
app.use('/static', express.static('public'));

// catch 404 and forward to error handler
//app.use(function(req, res, next) {
//  var err = new Error('Not Found');
//  err.status = 404;
//  next(err);
//});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: {}
  });
});

module.exports = app;
